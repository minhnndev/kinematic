package minhnn;

import java.util.Objects;

public class KinematicSeek {
	// Holds the static data for the character and target
	private Kinematic character;
	private Kinematic target;
	// Holds the maximum speed the character can travel
	private float maxSpeed;
	
	public KinematicSeek(Kinematic character, Kinematic target, float maxSpeed) {
		super();
		this.character = character;
		this.target = target;
		this.maxSpeed = maxSpeed;
	}

	public Kinematic getCharacter() {
		return character;
	}

	public void setCharacter(Kinematic character) {
		this.character = character;
	}

	public Kinematic getTarget() {
		return target;
	}

	public void setTarget(Kinematic target) {
		this.target = target;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((character == null) ? 0 : character.hashCode());
		result = prime * result + Float.floatToIntBits(maxSpeed);
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KinematicSeek other = (KinematicSeek) obj;
		if (character == null) {
			if (other.character != null)
				return false;
		} else if (!character.equals(other.character))
			return false;
		if (Float.floatToIntBits(maxSpeed) != Float.floatToIntBits(other.maxSpeed))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "KinematicSeek [character=" + character + ", target=" + target + ", maxSpeed=" + maxSpeed + "]";
	}

	public KinematicOutput ganerateKinematicOutput(){
		Vector2D velocity = new Vector2D();
		velocity = character.getPostision().sub(target.getPostision());
		velocity.normalize();
		velocity.mul(maxSpeed);
		return new KinematicOutput(velocity,0);
	}
	
}
