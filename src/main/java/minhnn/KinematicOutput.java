package minhnn;

import java.util.Objects;

public class KinematicOutput {
	private static Vector2D linear;
	private static float angular;
	
	public KinematicOutput(Vector2D linear, float angular) {
		super();
		this.linear = linear;
		this.angular = angular;
	}

	public static Vector2D getLinear() {
		return linear;
	}

	public void setLinear(Vector2D linear) {
		this.linear = linear;
	}

	public static float getAngular() {
		return angular;
	}

	public void setAngular(float angular) {
		this.angular = angular;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(angular);
		result = prime * result + ((linear == null) ? 0 : linear.hashCode());
		return result;
	}


	@Override
	public String toString() {
		return "KinematicOutput [linear=" + linear + ", angular=" + angular + "]";
	}

}
