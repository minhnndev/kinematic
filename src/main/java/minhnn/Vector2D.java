package minhnn;

import java.util.Objects;

public class Vector2D {
	private float x;
	private float z;

	public Vector2D(float x, float z) {
		// TODO Auto-generated constructor stub
		super();
		this.x = x;
		this.z = z;
	}

	public Vector2D() {}

	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		this.z = z;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(z);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2D other = (Vector2D) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vector2D [x=" + x + ", z=" + z + "]";
	}
	
	public Vector2D add(Vector2D b) {
		x+=b.getX();
		z+=b.getZ();
		return this;
	}
	
	public static Vector2D add(Vector2D b,Vector2D c) {
		return new Vector2D(b.getX() + c.getX(), b.getZ() + c.getZ());
	}

	public Vector2D sub(Vector2D b){
		this.setX(this.getX() - b.getX());
		this.setZ(this.getZ() - b.getZ());
		return this;
	}

	public Vector2D sub(Vector2D b, Vector2D c){
		return new Vector2D(b.getX() - c.getX(), b.getZ() - c.getZ());
	}

	public Vector2D mul(float constant){
		this.setX(this.getX()*constant);
		this.setZ(this.getZ()*constant);
		return this;
	}

	public Vector2D mul(Vector2D b, float constant){
		return new Vector2D(b.getX()*constant, b.getZ()*constant);
	}

	public double length(){
		return Math.sqrt(this.x*this.x+this.z*this.z);
	}

	public Vector2D normalize(){
		double length = this.length();
		this.x /=length;
		this.z /=length;
		return this;
	}

	public static void main(String[] args) {
		Vector2D a = new Vector2D(2,3);
		Vector2D b = new Vector2D(2,3);
		Vector2D c = new Vector2D(2,3);
		System.out.println(b.add(c).toString());
		System.out.println(b.sub(c).toString());
		System.out.println(b.mul(2).toString());
		System.out.println(b.length());
		System.out.println(b.normalize().toString());
		a.add(b).add(c);
	}
	
}
