package minhnn;

import java.util.Objects;

public class Kinematic {
	private static Vector2D postision; //vi tri
	private float orientation; //huong nhin
	private  static Vector2D velocity; // van toc
	private float rotation; // do xoay


	public Kinematic(Vector2D postision, float orientation, Vector2D velocity, float rotation) {
		super();
		this.postision = postision;
		this.orientation = orientation;
		this.velocity = velocity;
		this.rotation = rotation;
	}

	public static void setPostision(Vector2D postision) {
		Kinematic.postision = postision;
	}

	public void setOrientation(float orientation) {
		this.orientation = orientation;
	}

	public static void setVelocity(Vector2D velocity) {
		Kinematic.velocity = velocity;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	public Vector2D getPostision() {
		return postision;
	}

	public float getOrientation() {
		return orientation;
	}

	public static Vector2D getVelocity() {
		return velocity;
	}

	public float getRotation() {
		return rotation;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(orientation);
		result = prime * result + ((postision == null) ? 0 : postision.hashCode());
		result = prime * result + Float.floatToIntBits(rotation);
		result = prime * result + ((velocity == null) ? 0 : velocity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kinematic other = (Kinematic) obj;
		if (Float.floatToIntBits(orientation) != Float.floatToIntBits(other.orientation))
			return false;
		if (postision == null) {
			if (other.postision != null)
				return false;
		} else if (!postision.equals(other.postision))
			return false;
		if (Float.floatToIntBits(rotation) != Float.floatToIntBits(other.rotation))
			return false;
		if (velocity == null) {
			if (other.velocity != null)
				return false;
		} else if (!velocity.equals(other.velocity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Kinematic ["
				+ "postision=" + postision
				+ ", orientation=" + orientation
				+ ", velocity=" + velocity
				+ ", rotation=" + rotation + "]";
	}

	public void Update(KinematicOutput steering, float time) {
		float haft_t_sq = (float) (0.5 * time * time);
		this.postision = KinematicOutput.getLinear();
		this.rotation = KinematicOutput.getAngular();

		this.postision.add(this.velocity.mul(time));
		this.orientation += this.rotation * time;
	}

	public void applyNewOrientation() {
		if (this.velocity.length() > 0) {
			this.orientation = (float) Math.atan2(-this.velocity.getX(), this.rotation);
		}
	}
}